\documentclass[american,a4paper]{article}
\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage{uebungen}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{url}
\usepackage{float}

\lstset{stringstyle=\ttfamily, commentstyle=\it,
  basicstyle=\ttfamily,
  extendedchars=true,
  breakatwhitespace=true, breaklines=true, xleftmargin=.5cm}

\title{\textbf{DUNE-PDELab Exercise 2\\
    Wednesday, Feb~25$^\text{th}$~2015\\
    16:00-17:30}}
\dozent{Adrian Ngo / Jö Fahlke / Marian Piatkowski}
\institute{IWR, University of Heidelberg}
\semester{}
\abgabetitle{}
%\abgabeinfo{ in der Übung}
\uebungslabel{Exercise}
\blattlabel{DUNE Course 2015 Exercise}

\renewcommand{\thefootnote}{[\alph{footnote}]}

\newcommand{\vx}{\vec x}
\newcommand{\grad}{\vec \nabla}
\newcommand{\wind}{\vec \beta}
\newcommand{\Laplace}{\Delta}
\newcommand{\mycomment}[1]{}

\newenvironment{task}%
{\begin{list}{}{%
      \settowidth\leftmargin{$\Longrightarrow$}%
      \addtolength\leftmargin\labelsep%
      \setlength\rightmargin\leftmargin}%
  \bf\item[$\Longrightarrow$]}
{\end{list}}

\begin{document}

\blatt{}{}

\begin{uebung}{\bf Stationary problems with constraints}

In this exercise you will
\begin{itemize}
\item work on the constrained elliptic model problem from the lecture
\item implement the streamline diffusion method for the convection diffusion problem
\end{itemize}

\paragraph{Example 2a)}
The grid definition, solution scheme and local operator for solving the Laplace equation
\begin{equation*}
  \begin{aligned}
             -\Laplace u & = 0 && \text{ in } \Omega \\
                       u & = g && \text{ on } \Gamma_D\\
  -\grad u \cdot \vec\nu & = j && \text{ on } \Gamma_N
\end{aligned}
\end{equation*}
are implemented in the three files\\[\baselineskip]
\centerline{\tt example02.cc \hspace{1cm} example02\_Q1.hh \hspace{1cm}
  example02\_operator.hh}\\[\baselineskip]
In addition to this, there are two new header files\\[\baselineskip]
\centerline{\tt example02\_bctype.hh \hspace{1cm} example02\_bcextension.hh}
\\[\baselineskip]
which contain the implementation of the boundary condition type (Neumann or
Dirichlet) and the evaluation of the boundary function $g(\vx)$.

\begin{task}
  Get familiar with these two new files!  How is $g(\vx)$ defined and which
  part of the boundary is $\Gamma_D$ or $\Gamma_N$?  Build and run this
  example on a fine grid.
\end{task}

\paragraph{Example 2b)}
We replace the example 2a by the convection-diffusion problem:
\begin{equation*}
  \begin{aligned}
    \nabla \cdot \{ -\epsilon \grad u + u \wind \}
                                & = f         && \text{in } \Omega  \\
                              u & = g         && \text{on } \Gamma_D\\
       \{ -\epsilon \grad u + u \wind \} \cdot
                       \vec\nu  & = j         && \text{on } \Gamma_N\\
       -\epsilon \grad u \cdot
                        \vec\nu & = j_\epsilon && \text{on } \Gamma_\text{out}
   \end{aligned}
\end{equation*}
The Dirichlet b.c. can be set on any part of the domain boundary: $\Gamma_D \subset \partial \Omega.$
The remainder $\partial\Omega \setminus \Gamma_D$ is divided into a Neumann
boundary
\begin{equation*}
  \Gamma_N := \big\{ \vx \in \partial \Omega \setminus \Gamma_D
              \big|  \vec\nu(\vx)\cdot\beta \leq 0 \big\}
\end{equation*}
and an outflow boundary
\begin{equation*}
  \Gamma_{\text{out}} := \big\{ \vx \in \partial \Omega \setminus \Gamma_D
                       \big|  \vec\nu(\vx)\cdot\beta > 0 \big\}.
\end{equation*}

This equation arises in many models of flow transport processes.
The unknown function $u(\vx)$ may describe the concentration of a solute that is subject to diffusive effects (first term) and that is being carried along the streamlines
of the velocity field $\wind$ (second term). The problem is called
convection-dominant when the diffusion coefficient is very small ($\epsilon
\ll 1$).

We arbitrarily set the total flux over the Neumann boundary to $j=0$.
It doesn't matter really since in our case there will be no Neumann
boundary: $\Gamma_N=\emptyset$.  We also set the diffusive flux over
the outflow boundary to $j_\epsilon=0$.

\paragraph{Standard Galerkin Finite Element Method}
Take $w_h \in U_h^k$ with $w_h|_{\Gamma_D} = g$ and find $u_h \in w_h + \tilde U_h^k$  such that
\begin{equation*}
  \begin{aligned}
    r(u_h,v) &= 0 &&\forall v \in \tilde U_h^k
  \end{aligned}
\end{equation*}
where
\begin{equation*}
r(u,v) := \int \limits_{\Omega} \epsilon \grad u \cdot \grad v \,dx - \int \limits_{\Omega} u \wind \cdot \grad v \,dx
+ \int \limits_{\Gamma_\text{out}}
             \bigg\{ j_\epsilon + u \wind \cdot \vec\nu \bigg\} v \,ds
+ \int \limits_{\Gamma_N} j v \,ds
- \int \limits_{\Omega} f v \,dx
\end{equation*}

Open new header files\\[\baselineskip]
\centerline{\tt example02b\_Q1.hh \hspace{1cm} example02b\_operator.hh}
\centerline{\tt example02b\_bcextension.hh \hspace{1cm} example02b\_bctype.hh}
\\[\baselineskip]
which contain an implementation of this method with $Q_1$ elements.  Get
familiar with the code and find out which integral is implemented in which
method of the local operator class {\tt Example02bLocalOperator}.

{\bf Remark:} The boundary condition type has been extended to allow to
distinguish between Neumann and outflow boundaries in addition to Dirichlet
boundaries.  The discontinuous Dirichlet boundary function has been
regularized which improves the quality of the solution.  The velocity field is
given as $\wind = (1,\,0.25)^T$ and the source term is $f=0$.

\begin{task}
  Compile {\tt example02b.cc} and run the code!  See what happens for stronger
  and weaker convection (smaller and larger $\epsilon$, respectively).  A good
  starting point is $\epsilon=10^{-3}$.
\end{task}

\paragraph{Streamline Diffusion}
A better numerical scheme for this problem is given by the famous streamline
diffusion method which adds artificial diffusion in the streamline direction
of the velocity field (cf.\ {\it Elman-Silvester-Wathen: Finite elements and
  fast iterative solvers, Oxford University Press, 2005}).
The idea is to replace the test function $v$ by the term $v+\delta_{SD} \cdot \wind \cdot \grad v$ with
\begin{equation*}
  \delta_{SD} :=
    \begin{cases}
      \dfrac{h}{2\|\wind\|_2} \left(1-\dfrac{1}{P_h}\right)
                      & \text{for $P_h > 1$} \\[1em]
      0               & \text{for $P_h \leq 1$}
    \end{cases}
\end{equation*}
where the element Peclet number is defined as
\begin{equation*}
  P_h := \frac{ h }{2 \epsilon} \|\wind\|_2.
\end{equation*}

For $Q1$ elements, this adds the following terms to the weighted residual
formulation:
\begin{equation*}
  r_{SD}(u,v) := r(u,v)
    \,\,{\color{blue} + \int_\Omega \delta_{SD} (\wind \cdot \grad u)
                                              (\wind \cdot \grad v) \,dx }
    \,\,{\color{blue} - \int_\Omega f \delta_{SD} \wind \cdot \grad v \,dx }
\end{equation*}

\begin{task}
  Modify {\tt Example02bLocalOperator} to add the implementation of the
  coefficient $\delta_{SD}$ and the two new integrals!
\end{task}

{\bf Remark:} The code is written in such a way that the function {\tt void
  sdsolver(...)} is called twice.  The first time the LocalOperator is
instructed to omit the Streamline-Diffusion term, the second time it is
instructed to include them.

\begin{task}
  Compare the results with the standard Galerkin method!
\end{task}

\end{uebung}




%\newpage

\begin{figure}[H]
  \centering
\begin{minipage}[b]{.69\linewidth}
  \centering
  \includegraphics[width=\linewidth]{example02b_SG}
  %\caption{\label{fig:heterogoneous}heterogeneous diffusion field}
  %\vspace\baselineskip
\end{minipage}
\caption{Solution of 2b: Standard Galerkin for $\epsilon=10^{-6}$ and
  refinement level 7}
\end{figure}

\begin{figure}[H]
  \centering
\begin{minipage}[b]{.69\linewidth}
  \centering
  \includegraphics[width=\linewidth]{example02b_SD}
  %\caption{\label{fig:heterogoneous}heterogeneous diffusion field}
  %\vspace\baselineskip
\end{minipage}
\caption{Solution of 2b: Streamline diffusion $\epsilon=10^{-6}$ and
  refinement level 7}
\end{figure}

\newpage

\begin{uebung}{\bf Unstructured Meshes (optional)}
In this exercise you will
\begin{itemize}
\item read in unstructured meshes and
\item generate simple unstructured meshes.
\end{itemize}

A simple tool to generate a mesh from a geometry description is
Gmsh\footnote{This exercise is written with Gmsh 2.7.0 in mind, which is the
  version installed in the pool} (\url{http://geuz.org/gmsh/}).  In addition
to creating meshes it can also be used to actually create simple geometry
models.  More complex geometry models from CAD programs can be used via a
variety of input file formats.  Gmsh is developed by Christophe Geuzaine and
Jean-François Remacle with contributions by many others.

Gmsh consist of three modules: {\tt Geometry}, {\tt Mesh} and {\tt
  Solver}. In this exercise we are only concerned with the first two
modules.

\paragraph{Starting Gmsh}
If you're about to start Gmsh, you'll see to the left of the Gmsh window
the top level menu (compare with figure \ref{fig:top-level-menu}.
If you're familiar to older versions of Gmsh such as Gmsh 2.5.1, you'll
notice that navigating through the menus has been overworked. If you
click on the {\tt +}-icon right next to {\tt Geometry}, then the
submenus of {\tt Geometry} will pop out. The {\tt +}-icon changes to a
{\tt -}-icon and if you click again on this icon, the submenus will
disappear.

\paragraph{About the {\tt Geometry} Module}
The {\tt Geometry} module of Gmsh uses a simple scripting language.  A geometry
model has to be build up by first defining points, using these points to
define lines, using the lines to define surfaces, and finally using the
surfaces to define volumes.  This can be done by either connecting a set of
lower dimensional entities or by extruding them.  Of course you don't have to
go all the way: if all you need is a surface you don't have to define a
volume.

Gmsh's graphical user interface is mostly just a front-end to this scripting
language.  Every action you take in the GUI will generate a scripting command
that will be appended to the file you're editing.  Even deleting an entity
will append a delete command, so the entity will not really be gone from the
file.\footnote{Deleting e.g.\ points that are used to construct a line has no
  effect (not even an error message).  The delete command will be appended to
  the file you're editing, but the point will not be deleted.  You have to
  delete the line first.}  Especially when defining the initial points it is
often more convenient to open the {\tt .geo} file in a text editor and define
them manually than using the GUI.  Higher-dimensional entities are however
usually easier to define in the GUI.  You can switch back and forth between
the text editor and the Gmsh GUI at your leisure.  Of course, you must make
sure to reload the geometry file in your editor if you edited it in Gmsh, or
to reload it in Gmsh if you edited it in your editor (see figure
\ref{fig:reload}).

\begin{figure}[h]
  \centering
  \begin{minipage}[t]{0.45\linewidth}
    \centering
    \includegraphics[scale=.4]{gmsh-select-geometry}
    \caption{Top level menu right after starting Gmsh showing all the
      three modules {\tt Geometry}, {\tt Mesh} and {\tt Solver} }
    \label{fig:top-level-menu}
  \end{minipage}%
  \hspace{0.1\linewidth}%
  \begin{minipage}[t]{0.45\linewidth}
    \centering
    \includegraphics[scale=.4]{gmsh-geometry-reload}
    \caption{How to reload the Gmsh geometry file from the top-level button
      group of the {\tt Geometry} module.}
    \label{fig:reload}
  \end{minipage}
\end{figure}

{\bf Pitfall:} In this exercise we won't cover the physical entities.  Do not
define any.  If you do, only the elementary entities that have been assigned
to a physical entity will be meshed -- so if you have a 2D geometry and
defined a physical line for the boundary but no physical surface, you will get
a 1D-mesh of the boundary.  \footnote{Gmsh distinguishes between elementary
  and physical entities.  Elementary entities are the entities you use to
  build the geometry model.  You can group several elementary entities of the
  same dimension together to form a physical entity; each physical entity has
  a unique strictly positive integral number.  When writing a mesh file, the
  physical entity numbers are saved along with the mesh entities.  This can be
  used to define regions of interest in your domain: If you write your program
  to use Dirichlet boundary conditions for one physical entity number and
  Neumann boundary conditions for another, you can effectively define your
  {\tt BCType} function by point-and-click in the Gmsh-GUI.}

Geometry files have the filename extension {\tt .geo} and you can open them in
Gmsh by using the following command in the terminal:\footnote{Of course, you
  have to substitute the name your actual geometry file for {\tt sample.geo}.}
\begin{lstlisting}
gmsh sample.geo
\end{lstlisting}
Many other formats such as {\tt .brep} are also supported, at least for
reading and as input for mesh generation.

\paragraph{About the {\tt Mesh} Module}
Similar to the {\tt Geometry} module the {\tt Mesh} module builds the meshes
up from lower to higher dimensions.  It starts with 1D meshes by first meshing
the relevant lines, then meshes the 2D surfaces by using the 1D mesh as a
boundary mesh, and finally meshes the volumes by using the 2D meshes as a
boundary mesh, as appropriate.  It is even possible to use a 2D boundary mesh
that was generated by other means to generate a mesh of the volume it
encloses.

Mesh files have the extension {\tt .msh} and you can open and view them in
Gmsh's GUI just like geometry files:
\begin{lstlisting}
gmsh sample.msh
\end{lstlisting}

\paragraph{Example 2c)}
The main program in {\tt example02c.cc} has been copied from {\tt
  example02.cc} and modified to read a grid using the {\tt GmshReader}.
It also uses {\tt ALUSimplexGrid} instead of {\tt YaspGrid}, since the latter
only supports structured meshes.  The driver routine {\tt example02\_Q1.hh}
has been copied to {\tt example02c\_P1.hh} and modified to use $P_1$ elements
instead of $Q_1$, since {\tt ALUSimplexGrid} only supports simplex
meshes.\footnote{New versions of Gmsh have support for generating non-simplex
  meshes by recombining triangles into quadrilaterals for 2D, or by using
  structured meshing algorithms for 3D.  If you want to try it, you probably
  have to install a bleeding-edge version from Gmsh's homepage yourself, and
  you may find some bugs.  The {\tt .msh} file format however has support for
  non-simplex meshes for quiet some time, and you can load and inspect such
  meshes in the GUI.}

\paragraph{An Unstructured Unit Square}
Please start by reproducing example 2a with an unstructured mesh.  Have a look
at the file {\tt example02c.geo} in a text editor.  Right in the beginning
there is the line
\begin{lstlisting}
Mesh.CharacteristicLengthMax = 1.0/32;
\end{lstlisting}
This gives a limit to how big mesh entities may get.  If you want a finer
mesh, adjust this setting.  Next there are some point definitions like this:
\begin{lstlisting}
Point(1) = {0, 0, 0, 1e22};
\end{lstlisting}
This defines the elementary point with the number 1 at the position $(0,0,0)$.
It also defines the characteristic length of the mesh elements at that point
to $10^{22}$.\footnote{You can omit the characteristic length in the
  definition of the points.  Later versions of Gmsh (including version 2.4.2
  which is installed in the pool) will automatically use a value of $10^{22}$
  in that case.  We left the explicit value of $10^{22}$ in since older
  versions of Gmsh use
  $0$ as default value.  A characteristic length of $0$ will obviously lead to
  problems when used to generate a mesh.}  The value $10^{22}$ is chosen
more-or-less arbitrarily as some very large value.  The actual value used for
the characteristic length then results from restriction by the {\tt
  Mesh.CharacteristicLengthMax} option.

In the last block there are some line definitions:
\begin{lstlisting}
Line(1) = {1, 2};
\end{lstlisting}
This defines the elementary line with the number 1 to go straight from point 1
to point 2.

\begin{figure}
  \centering
  \tikz[baseline=-1.5cm]
    \node[below right] at (0,0) {%
      \includegraphics[origin=t,scale=.4]{gmsh-geometry-elementary-entities}%
    };
  $\longrightarrow$
  \tikz[baseline=-1.5cm]
    \node[below right] at (0,0) {%
      \includegraphics[scale=.4]{gmsh-geometry-elementary-entities-add}%
    };
  $\longrightarrow$
  \tikz[baseline=-1.5cm]
    \node[below right] at (0,0) {%
      \includegraphics[scale=.4]{gmsh-geometry-elementary-entities-add-new-plane-surface}%
    };
  \caption{Click-sequence to open the surface-creation dialog.}
  \label{fig:surface-creation}
\end{figure}
Now open the file using the Gmsh GUI:
\begin{lstlisting}
gmsh example02c.geo
\end{lstlisting}
You should see the unit square.  What is missing currently is the definition
of a surface bounded by the four lines.  To define it, click on {\tt
  Geometry} $\rightarrow$ {\tt Elementary entities} $\rightarrow$ {\tt Add}
$\rightarrow$ {\tt Plane surface}, see figure \ref{fig:surface-creation}.
The message
\begin{lstlisting}
Select surface boundary
[Press 'q' to abort]
\end{lstlisting}
should appear in the main window.  Click on one of the lines.  All four lines
should turn from blue to red, and the message should change to\footnote{When
  selecting a surface boundary by clicking on one of the lines, Gmsh
  automatically follows through all points that have only two connected
  lines.  If you have at least two points on the boundary that have more than
  one connected line, you will have to select additional parts the boundary
  explicitly.}
\begin{lstlisting}
Select hole boundaries (if none, press 'e')
[Press 'e' to end selection or 'q' to abort]
\end{lstlisting}
We don't want to define any holes, so just press 'e'.  Now you should have a
surface, indicated by the thin dashed cross.

Open the file in a text editor and have a look at it.  There should be
something like this at the end:
\begin{lstlisting}
Line Loop(5) = {1, 4, -2, -3};
Plane Surface(6) = {5};
\end{lstlisting}
A line loop defines a border.  A surface can be constructed out of one or more
borders: an outer border and some inner borders which define holes in the
surface.

\begin{figure}
  \centering
  \begin{minipage}[b]{0.45\linewidth}
    \centering
    \includegraphics[scale=.4]{gmsh-save-mesh}
    \caption{How to save the mesh.}
    \label{fig:mesh-save}
  \end{minipage}%
  \hspace{0.1\linewidth}%
  \begin{minipage}[b]{0.45\linewidth}
    \centering
    \includegraphics[scale=.4]{gmsh-options-mesh-general}
    \caption{Where to find the maximum characteristic length settings in the
      options.  It is called ``Maximum element size'' in the options dialog.}
    \label{fig:mesh-options}
  \end{minipage}
\end{figure}
\paragraph{Generating the Mesh}
To generate the mesh you can either use the command line or the GUI.  In the
GUI switch to the Mesh module by selecting it from the drop-down list.  You
will see are a lot of options, but for this exercise only the buttons {\tt 1D}
and {\tt 2D} are of interest.  {\tt 1D} generates the mesh on the lines and
{\tt 2D} on the whole surface (and on the lines, if you didn't do that before
explicitly).  Contrary to the geometry file, the mesh file is not saved
automatically, so
you must do that explicitly: The name of the mesh file will be the same as for
the geometry file with the extension replaced by {\tt .msh}.  The maximum
characteristic length, which was set in the first statement in the {\tt .geo}
file can also be adjusted via {\tt Tools} $\rightarrow$ {\tt Options}, see
figure \ref{fig:mesh-options}.\footnote{This is especially useful for file
  formats other than {\tt .geo} where you can't set it in the file itself.}

A mesh can also be generated from the command line.  To to so, run Gmsh like
this:
\begin{lstlisting}
gmsh -2 example02c.geo
\end{lstlisting}
The option {\tt -2} tell Gmsh to generate a 2D mesh.  Similar options {\tt -1}
and {\tt -3} are also available, to generate 1D and 3D meshes, respectively.
In any case, the mesh is automatically saved in a file with the same name as
the geometry file, with the extension replaced by {\tt .msh}.

\paragraph{Running the Simulation}

Once you have generated the mesh, go to one of the build directories,
compile {\tt example02c} and run
\begin{lstlisting}
./example02c ../../src/example02c.msh
\end{lstlisting}
You should get a {\tt .vtu} file. Open it and verify that the result looks
similar to one from example 2a.

\paragraph{Reminder} Be sure to remember that the {\tt .geo} file and also the
{\tt .msh} file you have just used in the simulation above, are located in the
source directory of {\tt pdelab-exercise2}. Therefore when executing the program
in one of the build directories, you need to \textbf{explicitly specify} the
relative path to the {\tt .msh} file. \newline
For instance, if you run the simulation with the command
\begin{lstlisting}
./example02c example02c.msh
\end{lstlisting}
the program terminates with an error message. You can try the command
to see how the error message looks like.

\begin{figure}
  \centering
  \centering
  \begin{tikzpicture}[x=3cm, y=3cm]
    \draw (0,0) rectangle (1,1); \foreach\c in{(0,0), (0,1), (1,0), (1,1)}
    \fill \c circle (2pt); \draw (0.5,0.5) circle (0.25); \foreach\c
    in{(0.25,0.5), (0.75,0.5), (0.5,0.25), (0.5,0.75)} \fill \c circle (2pt);
  \end{tikzpicture}
  \caption{A unit square domain with a hole.}
  \label{fig:unit-square-hole}
\end{figure}
\paragraph{A Unit Square with a Hole}
To make our example a little more interesting, we want to define a unit square
with a hole of radius $0.25$, like in figure \ref{fig:unit-square-hole}.  To
do this, we have to define a circular line.  We could of course define lots of
small straight line segments, after all the resulting mesh at that boundary
will consist of lots of small line segments anyway.  This is however
cumbersome.  It is also very restricting for the mesh generator, since it has
to place a mesh vertex exactly everywhere we place a point.

There are two better ways to do this: the {\tt Circle} and the {\tt Extrude}
commands.  We will show how to do this using extrusions, since the limitations
for our purpose are the same and extrusions are more generally useful.
Extrusions work by taking a point, a line, or a
surface and drawing it over the canvas in some way.  The line, surface, or
volume drawn is the resulting entity.

Have a look at {\tt example02c\_hole.geo} in a text editor.  It should have the
following content:
\begin{lstlisting}
// set characteristic length of elements
Mesh.CharacteristicLengthMax = 1.0/32;

// points are best entered manually
Point(1) = {0, 0, 0, 1e22};
\end{lstlisting}
As a warm-up to extrusions, we will use them to construct the unit square this
time.  Open the file in Gmsh and click {\tt Geometry} $\rightarrow$
{\tt Elementary entities} $\rightarrow$ {\tt Translate} $\rightarrow$
{\tt Extrude Point}
A dialog should appear where you can enter the direction and length of a
straight line along which the point is to be extruded. The main window should
display the message
\begin{lstlisting}
Select points
[Press 'e' to end selection or 'q' to abort]
\end{lstlisting}
Enter $(1,0,0)$ and click on the point in the main window, then press 'e'.
You should now have a single line.  Click on the button {\tt Extrude Line}
in the button list to and repeat the
process with the line using the extrusion vector $(0,1,0)$ to obtain a unit
square.

Reload the file in your text editor.  You should see the following additional lines:
\begin{lstlisting}
Extrude {1, 0, 0} {
  Point{1};
}
Extrude {0, 1, 0} {
  Line{1};
}
\end{lstlisting}
The keyword for all kinds of extrusions is {\tt Extrude} and is followed by
two pairs of braces (and there is no '{\tt ;}' required at the end).  The
second pair of braces always contains the elementary entities to be extruded.
The first pair specifies how to extrude -- in the case of extrusion by
translation it simply contains the components of the extrusion vector.

To create the hole we will use extrusion by rotation.  This works by
specifying the direction of the rotation axis (a vector whose length is
irrelevant), the position of the rotation axis (a point anywhere on it) and a
rotation angle (given in radians).  Gmsh limits the rotation angle to strictly
$<\pi$.\footnote{Gmsh won't complain if you use a rotation angle $\geq\pi$ but
  you will get surprising and/or unpredictable results.}  Because of this
limitation, we need at least three points on the circumference of the circular
hole. It is however easier to specify the four points given in figure
\ref{fig:unit-square-hole} on the boundary of the hole, so we will use
those.\footnote{The drawback of having points on the boundary of the hole is
  that the {\tt Mesh} module will place vertices of the grid there.  Current
  versions of Gmsh (2.6.1) offer the command {\tt Compound Line}, which can be
  used to create lines that do not suffer from this limitation.  There a also
  similar commands for surfaces and volumes.}

Edit the geometry file in the text editor an add the definition for those
points at the end.  Be sure to give each point a unique number -- the
extrusions will have implicitly added some points so it is probably best to
start the numbers somewhere around 10.

Once you have done that, go to the Gmsh GUI and reload the file (top-level
button group of the {\tt Geometry} module, button {\tt Reload}).  Verify that
the points were added correctly.  Click {\tt Elementary entities}
$\rightarrow$ {\tt Extrude} $\rightarrow$ {\tt Rotate} $\rightarrow$ {\tt
  Point} and enter $(0.5, 0.5, 0)$ as the axis point, $(0,0,1)$ as the axis
direction, and ``{\tt Pi/2}'' as the rotation angle.\footnote{Since the Gmsh
  GUI is just a front-end to generate Gmsh scripting commands, you can enter
  valid Gmsh scripting expressions almost anywhere.}  Select the four new
points and press 'e' to extrude them.

Inspect the file in the text editor.  You should see something similar to the
following lines at the end of the file:
\begin{lstlisting}
Extrude {{0, 0, 1}, {.5, .5, 0}, Pi/2} {
  Point{13, 11, 14, 12};
}
\end{lstlisting}
This extrude statement is similar to the ones above.  The first pair of braces
contains a different syntax: a vector in braces giving the direction of the
rotation axis, then a coordinate in braces giving the position of a point on
the rotation axis, and finally the rotation angle.\footnote{A combined
  translation/rotation extrusion statement is also possible.  Consult the Gmsh
  manual.}

Finally we need to define the surface.  Go to the Gmsh GUI and click {\tt
  Elementary entities} $\rightarrow$ {\tt Add} $\rightarrow$ {\tt New}
$\rightarrow$ {\tt Plane surface}.  Without doing anything else, you will
already see a faint dashed cross, indicating that there is already a surface.
What happened?  When you extruded the line, you already created the surface of
the square by drawing the line other the canvas.  This surface however also
covers the area of the hole, and we don't want that.  So before proceeding,
delete the already present surface using {\tt Elementary entities}
$\rightarrow$ {\tt Delete} $\rightarrow$ {\tt Surface}.  Now go to {\tt
  Elementary entities} $\rightarrow$ {\tt Add} $\rightarrow$ {\tt New}
$\rightarrow$ {\tt Plane surface} again, select first the outer square
boundary, then the inner hole boundary and finish surface creation by pressing
'e'.

Generate the mesh as before, then run
\begin{lstlisting}
./example02c ../../src/example02c_hole.msh
\end{lstlisting}
and inspect the result in ParaView.

\paragraph{The Power of Scripting}
Now we want to see what happens for different sizes of the hole.  You can
simply modify the coordinates on the circumference of the hole in the Gmsh
file, but doing that every time is error prone.  Instead you can define a
variable {\tt radius} and use that in the definition of the points:
\begin{lstlisting}
radius = 0.45;
\end{lstlisting}
For the $x$-coordinate of the point to the left of the hole you can then use
the expression \lstinline{0.5-radius}, for instance.  Adjust all the points of
the hole in this way, generate a new mesh, run the simulation, and inspect the
result in ParaView.  Vary the definition of the radius and repeat.

\end{uebung}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% mode: TeX-PDF
%%% TeX-master: t
%%% mode: flyspell
%%% ispell-local-dictionary: "american"
%%% End:
