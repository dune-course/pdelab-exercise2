// set characteristic length of elements
Mesh.CharacteristicLengthMax = 1.0/32;

// points are best entered manually
Point(1) = {0, 0, 0, 1e22};

