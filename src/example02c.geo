// set characteristic length of elements
Mesh.CharacteristicLengthMax = 1.0/32;

// points are best entered manually
Point(1) = {0, 0, 0, 1e22};
Point(2) = {1, 0, 0, 1e22};
Point(3) = {0, 1, 0, 1e22};
Point(4) = {1, 1, 0, 1e22};

// lines can also be entered via the GUI
Line(1) = {1, 2};
Line(2) = {3, 4};
Line(3) = {1, 3};
Line(4) = {2, 4};

// surfaces are best created in the GUI
